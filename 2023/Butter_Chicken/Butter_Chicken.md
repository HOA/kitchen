# Butter Chicken

**10 Personen**

---

- *10 Personen* [Makhani Sauce Basis](Makhani_Sauce_Basis.md)

## Marinade

- *100 g* Griechischer Joghurt
- *1 TL* Bockshornklee, gemahlen
- *1.5 EL* Garam Masala
- *8 g* Ingwer
- *3 TL* Salz
- *1.5 TL* Pfeffer gemahlen

## Hähnchen

- *1500 g* Hähnchenoberkeule ohne Haut, ohne Knochen
