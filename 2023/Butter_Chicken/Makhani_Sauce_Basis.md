# Makhani Sauce Basis

**10 Personen**

---

## Gewürzmischung

- *3 TL* Bockshornklee, gemahlen
- *1/8 TL* gemahlene Nelke
- *1/4 TL* gemahlener Kardamom
- *1 EL* Garam Masala (glutenfrei!)
- *1/2 EL* Salz

## Sauce

- *150 g* Zwiebeln
- *1/4 TL* Backnatron
- *3 cm³* Stück Ingwer
- *12 g* Knoblauch
- *30 g* Cashews, ungeröstet, ungesalzen
- *800 g* geschälte Tomaten
- *250 ml* Wasser
- *120 ml* Hafersahne
- *60 g* Alsan

## Zum Nachschärfen

- *1/2 TL* Cayenne Pfeffer

---

Etwas Sauce separat abnehmen und nicht mit der Einlage vermischen!