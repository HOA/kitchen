# Tomatensauce

**4 Personen**

---

- *2 EL* Öl
- *80 g* Zwiebel
- *1* Knoblauchzehen
- *20 g* Tomatenmark
- *400 g* Tomaten, geschält
- *20 g* Italienische Kräuter (TK)
- Salz
- Pfeffer
- Paprikapulver

---

Öl erhitzen, die Zwiebeln und den Knoblauch darin anbraten. Tomatenmark dazu geben, kurz durchrösten und mit den passierten Tomaten aufgießen. Ca. 30 min köcheln lassen, Kräuter dazugeben und mit Salz, Pfeffer und Paprikapulver abschmecken.