# Aufbaufrühstück


**20 Personen**

---

- *300 g* Alsan
- *150 g* Veganer Aufstrich (Gläser)

- *30 Scheiben* Käse
- *20 Scheiben* vegane Wurstalternative (z.B. Rügenwalder)

- *300 g* Nutella
- *200 g* Marmelade

- *500 g* Kaffeepulver
- *200 g* Zucker

- *500 ml* Milch
- *500 ml* Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
- *100 g* Gurken

- *8* Brötchen, weiß
- *27* Brötchen, Körner