# Frühstück

Frühstücksdinge.

**20 Personen**

---

- *300 g* Alsan
- *100 g* Frischkäse (gemischte Auswahl)
- *150 g* Veganer Aufstrich (Gläser)

- *15 Scheiben* Käse
- *5 Scheiben* Veganer Käse
- *10 Scheiben* Wurst
- *10 Scheiben* vegane Wurstalternative (z.B. Rügenwalder)
- *50 g* Fleischsalat

- *400 g* Müsli, diverse
- *300 g* Joghurt
- *100 g* Sojajoghurt (ungesüßt)

- *60 g* Erdnussbutter, creamy
- *60 g* Erdnussbutter, crunchy
- *300 g* Nutella
- *200 g* Marmelade
- *60 g* Pflaumenmus
- *100 g* Honig

- *4* Früchtetee-Beutel (gemischt)
- *4* Kräutertee-Beutel
- *4* Schwarz-/Grüntee-Beutel
- *500 g* Kaffeepulver
- *60 g* Kakaopulver
- *200 g* Zucker
- *5 Stk.* Süßstoff-Tabletten

- *1500 ml* Milch
- *1000 ml* Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
- *2500 ml* Orangensaft

- *400 g* Banane
- *400 g* Apfel

- *100 g* Cherry-Tomaten
- *300 g* Gurken

- *8* Brötchen, weiß
- *27* Brötchen, Körner