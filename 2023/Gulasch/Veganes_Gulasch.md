# Veganes Gulasch

**4 Personen**

---

- *300 g* Paprika mix
- *320 g* Kartoffeln frisch
- *320 ml* passierte Tomaten
- *320 ml* Tomaten in Stücken
- *80 ml* Rotwein (vegan)
- *100 g* Karotten
- *80 ml* Gemüsebrühe
- *4 g* Paprika edelsüss
- *2 g* Paprika rosenscharf
- *0.8 g* Pfeffer schwarz
- *10 g* Knoblauch
- *100 g* Zwiebeln
- Majoran
- Thymian
- Oregano
- Sojasauce
- Zucker

## Einlage

- *160 g* Sojaschnetzel
- *30 ml* CHEF Flüssiges Konzentrat Vegan Like Beef
- *50 ml* Rapsöl
- *40g* Tomatenmark 

---

## Zubereitung

- Brühe zubereiten (ca. 20ml Konzentrat/l).
- Sojaschnetzel in Brühe aufweichen
- Kartoffeln schälen und in kleine Stücke schneiden, parallel dazu:
- Sojaschnetzel abgießen und leicht auspressen
- Schnetzel in Öl anbraten, Tomatenmark  zugeben und kurz mitbraten
- Mit Rotwein ablöschen und in einen großen Topf geben
- Tomate dazu geben und langsam schmoren lassen
- Zwiebeln andünsten, evtl. mit Zucker bisschen karamellisieren, in den Topf geben
- Karotten und Paprika andünsten (und leicht bräunen/Farbe kriegen lassen) und dazu geben
- abschmecken
- Eine Stunde vor Ausgabe die rohen Kartoffeln dazu geben.
