# Wiener Wirtshausgulasch

Als Stärkebeilage traditionell Semmeln, alternativ geht aber auch Kartoffeln, Nudeln etc.

*beef, stew, fall, winter*

**4 Personen**

---

- *1 kg* Wadschunken (Hesse/Beinscheibe) vom Rind
- *50 g* Schweineschmalz
- *50 ml* Rapsöl
- *1 kg* Zwiebeln
- *5 g* Zucker
- *0,3 l* Bier, dunkles
- *15 g* Kümmel, ganz
- *15 g* Majoran, getrocknet, gerebelt
- *40 g* Paprikapulver, edelsüß
- *20 g* Paprikapulver, rosenscharf
- *15 ml* Branntweinessig

---

- Wadschunken in ca 4x4cm große Würfel schneiden 
- Zwiebeln mittelgrob würfeln (1-2 cm Kantenlänge)
- In Schweineschmalz in einzelner Lage scharf anbraten (nach Bedarf portionsweise)
- Fleisch beiseite stellen
- Zwiebeln in gleicher Pfanne anbraten bis sie goldbraun sind, am Ende der Bratzeit Zucker dazugeben
- Zwiebeln mit Fleisch, Fleischsaft und Bier in Topf geben und schmoren
- Gewürze & Essig zugeben, nach Bedarf mit Wasser aufgießen, dass das Fleisch zu zwei Drittel mit Flüssigkeit bedeckt ist

Quelle: [Frau Ziii](https://www.ziiikocht.at/2013/01/wiener-wirtshausgulasch-aus-dem.html)