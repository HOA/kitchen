-1: Sonntag | Aufbau-Frühstück | 450 g Alsan
-1: Sonntag | Aufbau-Frühstück | 225 g Veganer Aufstrich (Gläser)
-1: Sonntag | Aufbau-Frühstück | 45 Scheiben Käse
-1: Sonntag | Aufbau-Frühstück | 30 Scheiben vegane Wurstalternative (z.B. Rügenwalder)
-1: Sonntag | Aufbau-Frühstück | 450 g Nutella
-1: Sonntag | Aufbau-Frühstück | 300 g Marmelade
-1: Sonntag | Aufbau-Frühstück | 750 g Kaffeepulver
-1: Sonntag | Aufbau-Frühstück | 300 g Zucker
-1: Sonntag | Aufbau-Frühstück | 750 ml Milch
-1: Sonntag | Aufbau-Frühstück | 750 ml Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
-1: Sonntag | Aufbau-Frühstück | 150 g Gurken
0: Montag | Aufbau-Frühstück | 750 g Alsan
0: Montag | Aufbau-Frühstück | 375 g Veganer Aufstrich (Gläser)
0: Montag | Aufbau-Frühstück | 75 Scheiben Käse
0: Montag | Aufbau-Frühstück | 50 Scheiben vegane Wurstalternative (z.B. Rügenwalder)
0: Montag | Aufbau-Frühstück | 750 g Nutella
0: Montag | Aufbau-Frühstück | 500 g Marmelade
0: Montag | Aufbau-Frühstück | 1250 g Kaffeepulver
0: Montag | Aufbau-Frühstück | 500 g Zucker
0: Montag | Aufbau-Frühstück | 1250 ml Milch
0: Montag | Aufbau-Frühstück | 1250 ml Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
0: Montag | Aufbau-Frühstück | 250 g Gurken
1: Dienstag | Aufbau-Frühstück | 300 g Gurken
1: Dienstag | Aufbau-Frühstück | 900 g Alsan
1: Dienstag | Aufbau-Frühstück | 450 g Veganer Aufstrich (Gläser)
1: Dienstag | Aufbau-Frühstück | 90 Scheiben Käse
1: Dienstag | Aufbau-Frühstück | 60 Scheiben vegane Wurstalternative (z.B. Rügenwalder)
1: Dienstag | Aufbau-Frühstück | 900 g Nutella
1: Dienstag | Aufbau-Frühstück | 600 g Marmelade
1: Dienstag | Aufbau-Frühstück | 1500 g Kaffeepulver
1: Dienstag | Aufbau-Frühstück | 600 g Zucker
1: Dienstag | Aufbau-Frühstück | 1500 ml Milch
1: Dienstag | Aufbau-Frühstück | 1500 ml Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
-1: Sonntag | Abendessen | Frittierzeug zum Abendessen für Sonntag
0: Montag | Abendessen | 7.5 kg Vegetarische Tortelloni
0: Montag | Abendessen | 12.5 EL Öl
0: Montag | Abendessen | 500 g Zwiebel
0: Montag | Abendessen | 6.25 Knoblauchzehen
0: Montag | Abendessen | 125 g Tomatenmark
0: Montag | Abendessen | 2500 g Tomaten, geschält
0: Montag | Abendessen | 125 g Italienische Kräuter (TK)
0: Montag | Abendessen | Salz
0: Montag | Abendessen | Pfeffer
0: Montag | Abendessen | Paprikapulver
0: Montag | Abendessen | 625 ml Milch
0: Montag | Abendessen | 1250 ml Sahne
0: Montag | Abendessen | 1250 g Schmelzkäse
0: Montag | Abendessen | 62.5 g Petersilie (TK)
0: Montag | Abendessen | Muskat
0: Montag | Abendessen | Knoblauchpulver