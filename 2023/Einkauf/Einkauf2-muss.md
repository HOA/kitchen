
1: Dienstag | Abendessen | 12000 g Hähnchenoberkeule ohne Haut, ohne Knochen
1: Dienstag | Abendessen | 24000 g Rote Grütze
2: Mittwoch | Frühstück | 2550 g Alsan
2: Mittwoch | Frühstück | 850 g Frischkäse (gemischte Auswahl)
2: Mittwoch | Frühstück | 127.5 Scheiben Käse
2: Mittwoch | Frühstück | 42.5 Scheiben Veganer Käse
2: Mittwoch | Frühstück | 85 Scheiben Wurst
2: Mittwoch | Frühstück | 85 Scheiben vegane Wurstalternative (z.B. Rügenwalder)
2: Mittwoch | Frühstück | 425 g Fleischsalat
2: Mittwoch | Frühstück | 2550 g Joghurt
2: Mittwoch | Frühstück | 850 g Sojajoghurt (ungesüßt)
2: Mittwoch | Frühstück | 12750 ml Milch
2: Mittwoch | Frühstück | 8500 ml Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
2: Mittwoch | Frühstück | 21250 ml Orangensaft
2: Mittwoch | Frühstück | 3400 g Banane
2: Mittwoch | Frühstück | 3400 g Apfel
2: Mittwoch | Frühstück | 850 g Cherry-Tomaten
2: Mittwoch | Frühstück | 2550 g Gurken
2: Mittwoch | Abendessen | 3000 g Möhren
2: Mittwoch | Abendessen | 3100 g Paprika, rot
2: Mittwoch | Abendessen | 6000 g Kartoffeln
2: Mittwoch | Abendessen | 4000 g Tofu
2: Mittwoch | Abendessen | 10 Bio-Zitrone
2: Mittwoch | Abendessen | 160 g Gemüsebrühe (glutenfrei, sojafrei!)
2: Mittwoch | Abendessen | 2400 g Gewürzengurken
2: Mittwoch | Abendessen | 4000 g (Veganer) Frischkäse oder Crème fraîche
2: Mittwoch | Abendessen | 200 g Schnittlauch
2: Mittwoch | Abendessen | 200 g Petersilie
2: Mittwoch | Abendessen | 3000 g Möhren
2: Mittwoch | Abendessen | 3100 g Paprika, rot
2: Mittwoch | Abendessen | 6000 g Kartoffeln
2: Mittwoch | Abendessen | 1000 g Schinkenwürfel
2: Mittwoch | Abendessen | 3000 g Fleischwurst
2: Mittwoch | Abendessen | 10 Bio-Zitrone
2: Mittwoch | Abendessen | 160 g Gemüsebrühe (glutenfrei, sojafrei!)
2: Mittwoch | Abendessen | 2400 g Gewürzengurken
2: Mittwoch | Abendessen | 4000 g Frischkäse oder Crème fraîche
2: Mittwoch | Abendessen | 200 g Schnittlauch
2: Mittwoch | Abendessen | 200 g Petersilie
2: Mittwoch | Abendessen | 8000 g Gemischter Salat, geschnitten & gewaschen
2: Mittwoch | Abendessen | 1000 g Italienische Kräuter (TK)
2: Mittwoch | Abendessen | 200 g Gemüsebrühe (glutenfrei, sojafrei!)
3: Donnerstag | Frühstück | 2550 g Alsan
3: Donnerstag | Frühstück | 850 g Frischkäse (gemischte Auswahl)
3: Donnerstag | Frühstück | 127.5 Scheiben Käse
3: Donnerstag | Frühstück | 42.5 Scheiben Veganer Käse
3: Donnerstag | Frühstück | 85 Scheiben Wurst
3: Donnerstag | Frühstück | 85 Scheiben vegane Wurstalternative (z.B. Rügenwalder)
3: Donnerstag | Frühstück | 425 g Fleischsalat
3: Donnerstag | Frühstück | 2550 g Joghurt
3: Donnerstag | Frühstück | 850 g Sojajoghurt (ungesüßt)
3: Donnerstag | Frühstück | 12750 ml Milch
3: Donnerstag | Frühstück | 8500 ml Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
3: Donnerstag | Frühstück | 21250 ml Orangensaft
3: Donnerstag | Frühstück | 3400 g Banane
3: Donnerstag | Frühstück | 3400 g Apfel
3: Donnerstag | Frühstück | 850 g Cherry-Tomaten
3: Donnerstag | Frühstück | 2550 g Gurken
3: Donnerstag | Frühstück | 14875 ml Mandelmilch
3: Donnerstag | Frühstück | 5100 g Mehl
3: Donnerstag | Frühstück | 2550 g Dinkelmehl
3: Donnerstag | Frühstück | 170 g Vanillin-Zucker
3: Donnerstag | Frühstück | 340 g Backpulver