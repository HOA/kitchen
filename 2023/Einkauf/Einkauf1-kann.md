1: Dienstag | Abendessen | 1 TL gemahlene Nelke
1: Dienstag | Abendessen | 1 TL gemahlene Nelke
1: Dienstag | Abendessen | 100 g Pflanzenjoghurt ohne Soja
1: Dienstag | Abendessen | 12 EL Garam Masala
1: Dienstag | Abendessen | 12 EL Garam Masala
1: Dienstag | Abendessen | 12 TL Pfeffer gemahlen
1: Dienstag | Abendessen | 12 TL Pfeffer gemahlen
1: Dienstag | Abendessen | 1200 g Zwiebeln
1: Dienstag | Abendessen | 1200 g Zwiebeln
1: Dienstag | Abendessen | 2 TL Backnatron
1: Dienstag | Abendessen | 2 TL Backnatron
1: Dienstag | Abendessen | 2 TL gemahlener Kardamom
1: Dienstag | Abendessen | 2 TL gemahlener Kardamom
1: Dienstag | Abendessen | 20 kg Basmatireis
1: Dienstag | Abendessen | 24 cm³ Stück Ingwer
1: Dienstag | Abendessen | 24 cm³ Stück Ingwer
1: Dienstag | Abendessen | 24 TL Bockshornklee, gemahlen
1: Dienstag | Abendessen | 24 TL Bockshornklee, gemahlen
1: Dienstag | Abendessen | 24 TL Salz
1: Dienstag | Abendessen | 24 TL Salz
1: Dienstag | Abendessen | 240 g Cashews, ungeröstet, ungesalzen
1: Dienstag | Abendessen | 240 g Cashews, ungeröstet, ungesalzen
1: Dienstag | Abendessen | 4 EL Salz
1: Dienstag | Abendessen | 4 EL Salz
1: Dienstag | Abendessen | 4 TL Cayenne Pfeffer
1: Dienstag | Abendessen | 4 TL Cayenne Pfeffer
1: Dienstag | Abendessen | 480 g Alsan
1: Dienstag | Abendessen | 480 g Alsan
1: Dienstag | Abendessen | 4800 g Sojaschnetzel, mittelgrob
1: Dienstag | Abendessen | 64 g Ingwer
1: Dienstag | Abendessen | 64 g Ingwer
1: Dienstag | Abendessen | 6400 g geschälte Tomaten
1: Dienstag | Abendessen | 6400 g geschälte Tomaten
1: Dienstag | Abendessen | 8 EL Garam Masala (glutenfrei!)
1: Dienstag | Abendessen | 8 EL Garam Masala (glutenfrei!)
1: Dienstag | Abendessen | 8 TL Bockshornklee, gemahlen
1: Dienstag | Abendessen | 8 TL Bockshornklee, gemahlen
1: Dienstag | Abendessen | 800 g Griechischer Joghurt
1: Dienstag | Abendessen | 800 g Sojajoghurt
1: Dienstag | Abendessen | 96 g Knoblauch
1: Dienstag | Abendessen | 96 g Knoblauch
1: Dienstag | Abendessen | 960 ml Hafersahne
1: Dienstag | Abendessen | 960 ml Hafersahne
2: Mittwoch | Abendessen | 120 g Pfeffer
2: Mittwoch | Abendessen | 140 g Pfeffer
2: Mittwoch | Abendessen | 140 g Pfeffer
2: Mittwoch | Abendessen | 160 g Paprikapulver, edelsüß
2: Mittwoch | Abendessen | 160 g Paprikapulver, edelsüß
2: Mittwoch | Abendessen | 20 Zeh Knoblauch
2: Mittwoch | Abendessen | 200 g Senf, mittelscharf
2: Mittwoch | Abendessen | 2000 ml Weißweinessig
2: Mittwoch | Abendessen | 280 g Knoblauch
2: Mittwoch | Abendessen | 280 g Knoblauch
2: Mittwoch | Abendessen | 4 g Lorbeerblatt
2: Mittwoch | Abendessen | 4 g Lorbeerblatt
2: Mittwoch | Abendessen | 400 g Zucker
2: Mittwoch | Abendessen | 4000 ml Rapsöl
2: Mittwoch | Abendessen | 4400 g Zwiebeln
2: Mittwoch | Abendessen | 4400 g Zwiebeln
2: Mittwoch | Abendessen | 600 ml Rapsöl
2: Mittwoch | Abendessen | 600 ml Rapsöl
2: Mittwoch | Abendessen | 80 g Zucker
2: Mittwoch | Abendessen | 80 g Zucker
2: Mittwoch | Frühstück | 1275 g Veganer Aufstrich (Gläser)
2: Mittwoch | Frühstück | 1700 g Zucker
2: Mittwoch | Frühstück | 2550 g Nutella
2: Mittwoch | Frühstück | 34 Früchtetee-Beutel (gemischt)
2: Mittwoch | Frühstück | 34 Kräutertee-Beutel
2: Mittwoch | Frühstück | 34 Schwarz-/Grüntee-Beutel
2: Mittwoch | Frühstück | 3400 g Müsli, diverse
2: Mittwoch | Frühstück | 42.5 Stk. Süßstoff-Tabletten
2: Mittwoch | Frühstück | 4250 g Kaffeepulver
2: Mittwoch | Frühstück | 510 g Erdnussbutter, creamy
2: Mittwoch | Frühstück | 510 g Erdnussbutter, crunchy
2: Mittwoch | Frühstück | 510 g Kakaopulver
2: Mittwoch | Frühstück | 850 g Honig
3: Donnerstag | Abendessen | 13.33 EL Pfeffer, frisch gemahlen
3: Donnerstag | Abendessen | 1866.67 g Salz
3: Donnerstag | Abendessen | 2666.67 g Zucker
3: Donnerstag | Abendessen | 400 g Senf
3: Donnerstag | Abendessen | 466.67 g Zucker
3: Donnerstag | Abendessen | 933.33 ml Apfelessig
3: Donnerstag | Frühstück | 1275 g Veganer Aufstrich (Gläser)
3: Donnerstag | Frühstück | 1275 g Zucker
3: Donnerstag | Frühstück | 170 g Salz
3: Donnerstag | Frühstück | 1700 g Zucker
3: Donnerstag | Frühstück | 1700 ml neutrales Rapsöl
3: Donnerstag | Frühstück | 2550 g Nutella
3: Donnerstag | Frühstück | 34 Früchtetee-Beutel (gemischt)
3: Donnerstag | Frühstück | 34 Kräutertee-Beutel
3: Donnerstag | Frühstück | 34 Schwarz-/Grüntee-Beutel
3: Donnerstag | Frühstück | 3400 g Müsli, diverse
3: Donnerstag | Frühstück | 42.5 Stk. Süßstoff-Tabletten
3: Donnerstag | Frühstück | 4250 g Kaffeepulver
3: Donnerstag | Frühstück | 510 g Erdnussbutter, creamy
3: Donnerstag | Frühstück | 510 g Erdnussbutter, crunchy
3: Donnerstag | Frühstück | 510 g Kakaopulver
3: Donnerstag | Frühstück | 850 g Honig
4: Freitag | Abendessen | 100 g Zucker
4: Freitag | Abendessen | 1000 ml Rapsöl
4: Freitag | Abendessen | 1000 ml Rapsöl
4: Freitag | Abendessen | 16 g Pfeffer schwarz
4: Freitag | Abendessen | 1600 g Zucker
4: Freitag | Abendessen | 20 kg Zwiebeln
4: Freitag | Abendessen | 200 g Knoblauch
4: Freitag | Abendessen | 2000 g Zwiebeln
4: Freitag | Abendessen | 300 g Kümmel, ganz
4: Freitag | Abendessen | 300 g Majoran, getrocknet, gerebelt
4: Freitag | Abendessen | 300 ml Branntweinessig
4: Freitag | Abendessen | 40 g Paprika rosenscharf
4: Freitag | Abendessen | 400 g Paprikapulver, rosenscharf
4: Freitag | Abendessen | 768 g Zucker
4: Freitag | Abendessen | 80 g Paprika edelsüss
4: Freitag | Abendessen | 800 g Paprikapulver, edelsüß
4: Freitag | Abendessen | Majoran
4: Freitag | Abendessen | Oregano
4: Freitag | Abendessen | Salz
4: Freitag | Abendessen | Sojasauce
4: Freitag | Abendessen | Thymian
4: Freitag | Abendessen | Zucker
4: Freitag | Frühstück | 1275 g Veganer Aufstrich (Gläser)
4: Freitag | Frühstück | 1700 g Zucker
4: Freitag | Frühstück | 2550 g Nutella
4: Freitag | Frühstück | 34 Früchtetee-Beutel (gemischt)
4: Freitag | Frühstück | 34 Kräutertee-Beutel
4: Freitag | Frühstück | 34 Schwarz-/Grüntee-Beutel
4: Freitag | Frühstück | 3400 g Müsli, diverse
4: Freitag | Frühstück | 42.5 Stk. Süßstoff-Tabletten
4: Freitag | Frühstück | 4250 g Kaffeepulver
4: Freitag | Frühstück | 510 g Erdnussbutter, creamy
4: Freitag | Frühstück | 510 g Erdnussbutter, crunchy
4: Freitag | Frühstück | 510 g Kakaopulver
4: Freitag | Frühstück | 850 g Honig
5: Samstag | Frühstück | 1275 g Veganer Aufstrich (Gläser)
5: Samstag | Frühstück | 1700 g Zucker
5: Samstag | Frühstück | 2550 g Nutella
5: Samstag | Frühstück | 34 Früchtetee-Beutel (gemischt)
5: Samstag | Frühstück | 34 Kräutertee-Beutel
5: Samstag | Frühstück | 34 Schwarz-/Grüntee-Beutel
5: Samstag | Frühstück | 3400 g Müsli, diverse
5: Samstag | Frühstück | 42.5 Stk. Süßstoff-Tabletten
5: Samstag | Frühstück | 4250 g Kaffeepulver
5: Samstag | Frühstück | 510 g Erdnussbutter, creamy
5: Samstag | Frühstück | 510 g Erdnussbutter, crunchy
5: Samstag | Frühstück | 510 g Kakaopulver
5: Samstag | Frühstück | 850 g Honig
6: Sonntag | Abbau-Frühstück | 150 g Veganer Aufstrich (Gläser)
6: Sonntag | Abbau-Frühstück | 200 g Zucker
6: Sonntag | Abbau-Frühstück | 300 g Nutella
6: Sonntag | Abbau-Frühstück | 500 g Kaffeepulver
