3: Donnerstag | Abendessen | 160 Portionen Burger (50% Vegan)
3: Donnerstag | Abendessen | 200 g Petersilie
4: Freitag | Frühstück | 2550 g Alsan
4: Freitag | Frühstück | 850 g Frischkäse (gemischte Auswahl)
4: Freitag | Frühstück | 127.5 Scheiben Käse
4: Freitag | Frühstück | 42.5 Scheiben Veganer Käse
4: Freitag | Frühstück | 85 Scheiben Wurst
4: Freitag | Frühstück | 85 Scheiben vegane Wurstalternative (z.B. Rügenwalder)
4: Freitag | Frühstück | 425 g Fleischsalat
4: Freitag | Frühstück | 2550 g Joghurt
4: Freitag | Frühstück | 850 g Sojajoghurt (ungesüßt)
4: Freitag | Frühstück | 12750 ml Milch
4: Freitag | Frühstück | 8500 ml Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
4: Freitag | Frühstück | 21250 ml Orangensaft
4: Freitag | Frühstück | 3400 g Banane
4: Freitag | Frühstück | 3400 g Apfel
4: Freitag | Frühstück | 850 g Cherry-Tomaten
4: Freitag | Frühstück | 2550 g Gurken
4: Freitag | Frühstück | 3.54 kg Große Dose Bohnen
4: Freitag | Frühstück | 11.33 l Vollei
4: Freitag | Abendessen | 20 kg Wadschunken (Hesse/Beinscheibe) vom Rind
4: Freitag | Abendessen | 1000 g Schweineschmalz
4: Freitag | Abendessen | 6 l Bier, dunkles
4: Freitag | Abendessen | 6000 g Paprika mix
4: Freitag | Abendessen | 6400 ml passierte Tomaten
4: Freitag | Abendessen | 6400 ml Tomaten in Stücken
4: Freitag | Abendessen | 1600 ml Rotwein (vegan)
4: Freitag | Abendessen | 1600 ml Gemüsebrühe
4: Freitag | Abendessen | 3200 g Sojaschnetzel
4: Freitag | Abendessen | 20000 g Pasta
4: Freitag | Abendessen | 8000 g Milchreis
4: Freitag | Abendessen | 32 l Haferdrink, ca. 1.8% Fett
4: Freitag | Abendessen | 10.67 Zitrone, Zeste davon
4: Freitag | Abendessen | 3200 g Fruchtcocktail (Abtropfgewicht)
4: Freitag | Abendessen | 1600 g Apfelkompott
4: Freitag | Abendessen | 3200 g Schattenmorellen
5: Samstag | Frühstück | 2550 g Alsan
5: Samstag | Frühstück | 850 g Frischkäse (gemischte Auswahl)
5: Samstag | Frühstück | 127.5 Scheiben Käse
5: Samstag | Frühstück | 42.5 Scheiben Veganer Käse
5: Samstag | Frühstück | 85 Scheiben Wurst
5: Samstag | Frühstück | 85 Scheiben vegane Wurstalternative (z.B. Rügenwalder)
5: Samstag | Frühstück | 425 g Fleischsalat
5: Samstag | Frühstück | 2550 g Joghurt
5: Samstag | Frühstück | 850 g Sojajoghurt (ungesüßt)
5: Samstag | Frühstück | 12750 ml Milch
5: Samstag | Frühstück | 8500 ml Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
5: Samstag | Frühstück | 21250 ml Orangensaft
5: Samstag | Frühstück | 3400 g Banane
5: Samstag | Frühstück | 3400 g Apfel
5: Samstag | Frühstück | 850 g Cherry-Tomaten
5: Samstag | Frühstück | 2550 g Gurken
6: Sonntag | Abbau-Frühstück | 300 g Alsan
6: Sonntag | Abbau-Frühstück | 30 Scheiben Käse
6: Sonntag | Abbau-Frühstück | 20 Scheiben vegane Wurstalternative (z.B. Rügenwalder)
6: Sonntag | Abbau-Frühstück | 500 ml Milch
6: Sonntag | Abbau-Frühstück | 500 ml Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
