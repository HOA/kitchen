## Schon bestellt

Sonntag: Aufbau-Frühstück | 200 g Marmelade
Montag: Aufbau-Frühstück | 350 g Marmelade
Dienstag: Aufbau-Frühstück | 500 g Marmelade
Mittwoch: Frühstück | 1700 g Marmelade
Donnerstag: Frühstück | 1700 g Marmelade
Freitag: Frühstück | 1700 g Marmelade
Samstag: Frühstück | 1700 g Marmelade
Sonntag: Abbau-Frühstück | 200 g Marmelade

Dienstag: Abendessen | 4800 g Sojaschnetzel, mittelgrob
Freitag: Abendessen | 3200 g Sojaschnetzel

Dienstag: Abendessen | 800 ml CHEF Flüssiges Konzentrat Vegan Like Chicken
Freitag: Abendessen | 600 ml CHEF Flüssiges Konzentrat Vegan Like Beef

Sonntag: Aufbau-Frühstück | 12 Brötchen, weiß
Sonntag: Aufbau-Frühstück | 40.5 Brötchen, Körner
Montag: Aufbau-Frühstück | 20 Brötchen, weiß
Montag: Aufbau-Frühstück | 67.5 Brötchen, Körner
Dienstag: Aufbau-Frühstück | 24 Brötchen, weiß
Dienstag: Aufbau-Frühstück | 81 Brötchen, Körner
Mittwoch: Frühstück | 68 Brötchen, weiß
Mittwoch: Frühstück | 229.5 Brötchen, Körner
Donnerstag: Frühstück | 68 Brötchen, weiß
Donnerstag: Frühstück | 229.5 Brötchen, Körner
Freitag: Frühstück | 68 Brötchen, weiß
Freitag: Frühstück | 229.5 Brötchen, Körner
Samstag: Frühstück | 68 Brötchen, weiß
Samstag: Frühstück | 229.5 Brötchen, Körner
Sonntag: Abbau-Frühstück | 8 Brötchen, weiß
Sonntag: Abbau-Frühstück | 27 Brötchen, Körner