-1: Sonntag | Aufbau-Frühstück | 450 g Alsan
-1: Sonntag | Aufbau-Frühstück | 225 g Veganer Aufstrich (Gläser)
-1: Sonntag | Aufbau-Frühstück | 45 Scheiben Käse
-1: Sonntag | Aufbau-Frühstück | 30 Scheiben vegane Wurstalternative (z.B. Rügenwalder)
-1: Sonntag | Aufbau-Frühstück | 450 g Nutella
-1: Sonntag | Aufbau-Frühstück | 300 g Marmelade
-1: Sonntag | Aufbau-Frühstück | 750 g Kaffeepulver
-1: Sonntag | Aufbau-Frühstück | 300 g Zucker
-1: Sonntag | Aufbau-Frühstück | 750 ml Milch
-1: Sonntag | Aufbau-Frühstück | 750 ml Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
-1: Sonntag | Aufbau-Frühstück | 150 g Gurken
-1: Sonntag | Aufbau-Frühstück | 12 Brötchen, weiß
-1: Sonntag | Aufbau-Frühstück | 40.5 Brötchen, Körner
-1: Sonntag | Abendessen | Frittierzeug zum Abendessen für Sonntag
0: Montag | Aufbau-Frühstück | 750 g Alsan
0: Montag | Aufbau-Frühstück | 375 g Veganer Aufstrich (Gläser)
0: Montag | Aufbau-Frühstück | 75 Scheiben Käse
0: Montag | Aufbau-Frühstück | 50 Scheiben vegane Wurstalternative (z.B. Rügenwalder)
0: Montag | Aufbau-Frühstück | 750 g Nutella
0: Montag | Aufbau-Frühstück | 500 g Marmelade
0: Montag | Aufbau-Frühstück | 1250 g Kaffeepulver
0: Montag | Aufbau-Frühstück | 500 g Zucker
0: Montag | Aufbau-Frühstück | 1250 ml Milch
0: Montag | Aufbau-Frühstück | 1250 ml Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
0: Montag | Aufbau-Frühstück | 250 g Gurken
0: Montag | Aufbau-Frühstück | 20 Brötchen, weiß
0: Montag | Aufbau-Frühstück | 67.5 Brötchen, Körner
0: Montag | Abendessen | 7.5 kg Vegetarische Tortelloni
0: Montag | Abendessen | 2.5 kg Vegane Tortelloni
0: Montag | Abendessen | 12.5 EL Öl
0: Montag | Abendessen | 500 g Zwiebel
0: Montag | Abendessen | 6.25 Knoblauchzehen
0: Montag | Abendessen | 125 g Tomatenmark
0: Montag | Abendessen | 2500 g Tomaten, geschält
0: Montag | Abendessen | 125 g Italienische Kräuter (TK)
0: Montag | Abendessen | Salz
0: Montag | Abendessen | Pfeffer
0: Montag | Abendessen | Paprikapulver
0: Montag | Abendessen | 625 ml Milch
0: Montag | Abendessen | 1250 ml Sahne
0: Montag | Abendessen | 1250 g Schmelzkäse
0: Montag | Abendessen | 62.5 g Petersilie (TK)
0: Montag | Abendessen | Muskat
0: Montag | Abendessen | Knoblauchpulver
1: Dienstag | Aufbau-Frühstück | 900 g Alsan
1: Dienstag | Aufbau-Frühstück | 450 g Veganer Aufstrich (Gläser)
1: Dienstag | Aufbau-Frühstück | 90 Scheiben Käse
1: Dienstag | Aufbau-Frühstück | 60 Scheiben vegane Wurstalternative (z.B. Rügenwalder)
1: Dienstag | Aufbau-Frühstück | 900 g Nutella
1: Dienstag | Aufbau-Frühstück | 600 g Marmelade
1: Dienstag | Aufbau-Frühstück | 1500 g Kaffeepulver
1: Dienstag | Aufbau-Frühstück | 600 g Zucker
1: Dienstag | Aufbau-Frühstück | 1500 ml Milch
1: Dienstag | Aufbau-Frühstück | 1500 ml Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
1: Dienstag | Aufbau-Frühstück | 300 g Gurken
1: Dienstag | Aufbau-Frühstück | 24 Brötchen, weiß
1: Dienstag | Aufbau-Frühstück | 81 Brötchen, Körner
1: Dienstag | Abendessen | 100 g Pflanzenjoghurt ohne Soja
1: Dienstag | Abendessen | 60 g Erbesenprotein-Schnetzel
1: Dienstag | Abendessen | 20 kg Basmatireis
1: Dienstag | Abendessen | 24 TL Bockshornklee, gemahlen
1: Dienstag | Abendessen | 1 TL gemahlene Nelke
1: Dienstag | Abendessen | 2 TL gemahlener Kardamom
1: Dienstag | Abendessen | 8 EL Garam Masala (glutenfrei!)
1: Dienstag | Abendessen | 4 EL Salz
1: Dienstag | Abendessen | 1200 g Zwiebeln
1: Dienstag | Abendessen | 2 TL Backnatron
1: Dienstag | Abendessen | 24 cm³ Stück Ingwer
1: Dienstag | Abendessen | 96 g Knoblauch
1: Dienstag | Abendessen | 240 g Cashews, ungeröstet, ungesalzen
1: Dienstag | Abendessen | 6400 g geschälte Tomaten
1: Dienstag | Abendessen | 2000 ml Wasser
1: Dienstag | Abendessen | 960 ml Hafersahne
1: Dienstag | Abendessen | 480 g Alsan
1: Dienstag | Abendessen | 4 TL Cayenne Pfeffer
1: Dienstag | Abendessen | 800 g Sojajoghurt
1: Dienstag | Abendessen | 8 TL Bockshornklee, gemahlen
1: Dienstag | Abendessen | 12 EL Garam Masala
1: Dienstag | Abendessen | 64 g Ingwer
1: Dienstag | Abendessen | 24 TL Salz
1: Dienstag | Abendessen | 12 TL Pfeffer gemahlen
1: Dienstag | Abendessen | 4800 g Sojaschnetzel, mittelgrob
1: Dienstag | Abendessen | 800 ml CHEF Flüssiges Konzentrat Vegan Like Chicken
1: Dienstag | Abendessen | 24 TL Bockshornklee, gemahlen
1: Dienstag | Abendessen | 1 TL gemahlene Nelke
1: Dienstag | Abendessen | 2 TL gemahlener Kardamom
1: Dienstag | Abendessen | 8 EL Garam Masala (glutenfrei!)
1: Dienstag | Abendessen | 4 EL Salz
1: Dienstag | Abendessen | 1200 g Zwiebeln
1: Dienstag | Abendessen | 2 TL Backnatron
1: Dienstag | Abendessen | 24 cm³ Stück Ingwer
1: Dienstag | Abendessen | 96 g Knoblauch
1: Dienstag | Abendessen | 240 g Cashews, ungeröstet, ungesalzen
1: Dienstag | Abendessen | 6400 g geschälte Tomaten
1: Dienstag | Abendessen | 2000 ml Wasser
1: Dienstag | Abendessen | 960 ml Hafersahne
1: Dienstag | Abendessen | 480 g Alsan
1: Dienstag | Abendessen | 4 TL Cayenne Pfeffer
1: Dienstag | Abendessen | 800 g Griechischer Joghurt
1: Dienstag | Abendessen | 8 TL Bockshornklee, gemahlen
1: Dienstag | Abendessen | 12 EL Garam Masala
1: Dienstag | Abendessen | 64 g Ingwer
1: Dienstag | Abendessen | 24 TL Salz
1: Dienstag | Abendessen | 12 TL Pfeffer gemahlen
1: Dienstag | Abendessen | 12000 g Hähnchenoberkeule ohne Haut, ohne Knochen
1: Dienstag | Abendessen | 24000 g Rote Grütze
2: Mittwoch | Frühstück | 2550 g Alsan
2: Mittwoch | Frühstück | 850 g Frischkäse (gemischte Auswahl)
2: Mittwoch | Frühstück | 1275 g Veganer Aufstrich (Gläser)
2: Mittwoch | Frühstück | 127.5 Scheiben Käse
2: Mittwoch | Frühstück | 42.5 Scheiben Veganer Käse
2: Mittwoch | Frühstück | 85 Scheiben Wurst
2: Mittwoch | Frühstück | 85 Scheiben vegane Wurstalternative (z.B. Rügenwalder)
2: Mittwoch | Frühstück | 425 g Fleischsalat
2: Mittwoch | Frühstück | 3400 g Müsli, diverse
2: Mittwoch | Frühstück | 2550 g Joghurt
2: Mittwoch | Frühstück | 850 g Sojajoghurt (ungesüßt)
2: Mittwoch | Frühstück | 510 g Erdnussbutter, creamy
2: Mittwoch | Frühstück | 510 g Erdnussbutter, crunchy
2: Mittwoch | Frühstück | 2550 g Nutella
2: Mittwoch | Frühstück | 1700 g Marmelade
2: Mittwoch | Frühstück | 510 g Pflaumenmus
2: Mittwoch | Frühstück | 850 g Honig
2: Mittwoch | Frühstück | 34 Früchtetee-Beutel (gemischt)
2: Mittwoch | Frühstück | 34 Kräutertee-Beutel
2: Mittwoch | Frühstück | 34 Schwarz-/Grüntee-Beutel
2: Mittwoch | Frühstück | 4250 g Kaffeepulver
2: Mittwoch | Frühstück | 510 g Kakaopulver
2: Mittwoch | Frühstück | 1700 g Zucker
2: Mittwoch | Frühstück | 42.5 Stk. Süßstoff-Tabletten
2: Mittwoch | Frühstück | 12750 ml Milch
2: Mittwoch | Frühstück | 8500 ml Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
2: Mittwoch | Frühstück | 21250 ml Orangensaft
2: Mittwoch | Frühstück | 3400 g Banane
2: Mittwoch | Frühstück | 3400 g Apfel
2: Mittwoch | Frühstück | 850 g Cherry-Tomaten
2: Mittwoch | Frühstück | 2550 g Gurken
2: Mittwoch | Frühstück | 68 Brötchen, weiß
2: Mittwoch | Frühstück | 229.5 Brötchen, Körner
2: Mittwoch | Abendessen | 3000 g Möhren
2: Mittwoch | Abendessen | 3100 g Paprika, rot
2: Mittwoch | Abendessen | 6000 g Kartoffeln
2: Mittwoch | Abendessen | 4400 g Zwiebeln
2: Mittwoch | Abendessen | 4000 g Tofu
2: Mittwoch | Abendessen | 600 g Tomatenmark
2: Mittwoch | Abendessen | 280 g Knoblauch
2: Mittwoch | Abendessen | 600 ml Öl
2: Mittwoch | Abendessen | 4 g Lorbeerblatt
2: Mittwoch | Abendessen | 10 Bio-Zitrone
2: Mittwoch | Abendessen | 10000 ml Wasser
2: Mittwoch | Abendessen | 140 g Pfeffer
2: Mittwoch | Abendessen | 160 g Paprikapulver, edelsüß
2: Mittwoch | Abendessen | 80 g Zucker
2: Mittwoch | Abendessen | 160 g Gemüsebrühe (glutenfrei, sojafrei!)
2: Mittwoch | Abendessen | 2400 g Gewürzengurken
2: Mittwoch | Abendessen | 4000 g (Veganer) Frischkäse oder Crème fraîche
2: Mittwoch | Abendessen | 200 g Schnittlauch
2: Mittwoch | Abendessen | 200 g Petersilie
2: Mittwoch | Abendessen | 3000 g Möhren
2: Mittwoch | Abendessen | 3100 g Paprika, rot
2: Mittwoch | Abendessen | 6000 g Kartoffeln
2: Mittwoch | Abendessen | 4400 g Zwiebeln
2: Mittwoch | Abendessen | 1000 g Schinkenwürfel
2: Mittwoch | Abendessen | 3000 g Fleischwurst
2: Mittwoch | Abendessen | 600 g Tomatenmark
2: Mittwoch | Abendessen | 280 g Knoblauch
2: Mittwoch | Abendessen | 600 ml Öl
2: Mittwoch | Abendessen | 4 g Lorbeerblatt
2: Mittwoch | Abendessen | 10 Bio-Zitrone
2: Mittwoch | Abendessen | 10000 ml Wasser
2: Mittwoch | Abendessen | 140 g Pfeffer
2: Mittwoch | Abendessen | 160 g Paprikapulver, edelsüß
2: Mittwoch | Abendessen | 80 g Zucker
2: Mittwoch | Abendessen | 160 g Gemüsebrühe (glutenfrei, sojafrei!)
2: Mittwoch | Abendessen | 2400 g Gewürzengurken
2: Mittwoch | Abendessen | 4000 g Frischkäse oder Crème fraîche
2: Mittwoch | Abendessen | 200 g Schnittlauch
2: Mittwoch | Abendessen | 200 g Petersilie
2: Mittwoch | Abendessen | 8000 g Gemischter Salat, geschnitten & gewaschen
2: Mittwoch | Abendessen | 200 g Senf, mittelscharf
2: Mittwoch | Abendessen | 2000 ml Weißweinessig
2: Mittwoch | Abendessen | 4000 ml Neutrales Rapsöl
2: Mittwoch | Abendessen | 1000 g Italienische Kräuter (TK)
2: Mittwoch | Abendessen | 20 Zeh Knoblauch
2: Mittwoch | Abendessen | 200 g Gemüsebrühe (glutenfrei, sojafrei!)
2: Mittwoch | Abendessen | 120 g Pfeffer
2: Mittwoch | Abendessen | 400 g Zucker
3: Donnerstag | Frühstück | 2550 g Alsan
3: Donnerstag | Frühstück | 850 g Frischkäse (gemischte Auswahl)
3: Donnerstag | Frühstück | 1275 g Veganer Aufstrich (Gläser)
3: Donnerstag | Frühstück | 127.5 Scheiben Käse
3: Donnerstag | Frühstück | 42.5 Scheiben Veganer Käse
3: Donnerstag | Frühstück | 85 Scheiben Wurst
3: Donnerstag | Frühstück | 85 Scheiben vegane Wurstalternative (z.B. Rügenwalder)
3: Donnerstag | Frühstück | 425 g Fleischsalat
3: Donnerstag | Frühstück | 3400 g Müsli, diverse
3: Donnerstag | Frühstück | 2550 g Joghurt
3: Donnerstag | Frühstück | 850 g Sojajoghurt (ungesüßt)
3: Donnerstag | Frühstück | 510 g Erdnussbutter, creamy
3: Donnerstag | Frühstück | 510 g Erdnussbutter, crunchy
3: Donnerstag | Frühstück | 2550 g Nutella
3: Donnerstag | Frühstück | 1700 g Marmelade
3: Donnerstag | Frühstück | 510 g Pflaumenmus
3: Donnerstag | Frühstück | 850 g Honig
3: Donnerstag | Frühstück | 34 Früchtetee-Beutel (gemischt)
3: Donnerstag | Frühstück | 34 Kräutertee-Beutel
3: Donnerstag | Frühstück | 34 Schwarz-/Grüntee-Beutel
3: Donnerstag | Frühstück | 4250 g Kaffeepulver
3: Donnerstag | Frühstück | 510 g Kakaopulver
3: Donnerstag | Frühstück | 1700 g Zucker
3: Donnerstag | Frühstück | 42.5 Stk. Süßstoff-Tabletten
3: Donnerstag | Frühstück | 12750 ml Milch
3: Donnerstag | Frühstück | 8500 ml Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
3: Donnerstag | Frühstück | 21250 ml Orangensaft
3: Donnerstag | Frühstück | 3400 g Banane
3: Donnerstag | Frühstück | 3400 g Apfel
3: Donnerstag | Frühstück | 850 g Cherry-Tomaten
3: Donnerstag | Frühstück | 2550 g Gurken
3: Donnerstag | Frühstück | 68 Brötchen, weiß
3: Donnerstag | Frühstück | 229.5 Brötchen, Körner
3: Donnerstag | Frühstück | 14875 ml Mandelmilch
3: Donnerstag | Frühstück | 5100 g Mehl
3: Donnerstag | Frühstück | 2550 g Dinkelmehl
3: Donnerstag | Frühstück | 170 g Vanillin-Zucker
3: Donnerstag | Frühstück | 340 g Backpulver
3: Donnerstag | Frühstück | 1700 ml neutrales Öl
3: Donnerstag | Frühstück | 1275 g Zucker
3: Donnerstag | Frühstück | 170 g Salz
3: Donnerstag | Abendessen | 160 Portionen Burger (50% Vegan)
3: Donnerstag | Abendessen | 26.67 kg Weißkohl
3: Donnerstag | Abendessen | 1666.67 g Karotten
3: Donnerstag | Abendessen | 1333.33 g rote Zwiebel
3: Donnerstag | Abendessen | 200 g Petersilie
3: Donnerstag | Abendessen | 2666.67 g Zucker
3: Donnerstag | Abendessen | 1866.67 g Salz
3: Donnerstag | Abendessen | 2666.67 g vegane Mayonnaise
3: Donnerstag | Abendessen | 933.33 ml Apfelessig
3: Donnerstag | Abendessen | 400 g Senf
3: Donnerstag | Abendessen | 13.33 EL Pfeffer, frisch gemahlen
3: Donnerstag | Abendessen | 466.67 g Zucker
4: Freitag | Frühstück | 2550 g Alsan
4: Freitag | Frühstück | 850 g Frischkäse (gemischte Auswahl)
4: Freitag | Frühstück | 1275 g Veganer Aufstrich (Gläser)
4: Freitag | Frühstück | 127.5 Scheiben Käse
4: Freitag | Frühstück | 42.5 Scheiben Veganer Käse
4: Freitag | Frühstück | 85 Scheiben Wurst
4: Freitag | Frühstück | 85 Scheiben vegane Wurstalternative (z.B. Rügenwalder)
4: Freitag | Frühstück | 425 g Fleischsalat
4: Freitag | Frühstück | 3400 g Müsli, diverse
4: Freitag | Frühstück | 2550 g Joghurt
4: Freitag | Frühstück | 850 g Sojajoghurt (ungesüßt)
4: Freitag | Frühstück | 510 g Erdnussbutter, creamy
4: Freitag | Frühstück | 510 g Erdnussbutter, crunchy
4: Freitag | Frühstück | 2550 g Nutella
4: Freitag | Frühstück | 1700 g Marmelade
4: Freitag | Frühstück | 510 g Pflaumenmus
4: Freitag | Frühstück | 850 g Honig
4: Freitag | Frühstück | 34 Früchtetee-Beutel (gemischt)
4: Freitag | Frühstück | 34 Kräutertee-Beutel
4: Freitag | Frühstück | 34 Schwarz-/Grüntee-Beutel
4: Freitag | Frühstück | 4250 g Kaffeepulver
4: Freitag | Frühstück | 510 g Kakaopulver
4: Freitag | Frühstück | 1700 g Zucker
4: Freitag | Frühstück | 42.5 Stk. Süßstoff-Tabletten
4: Freitag | Frühstück | 12750 ml Milch
4: Freitag | Frühstück | 8500 ml Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
4: Freitag | Frühstück | 21250 ml Orangensaft
4: Freitag | Frühstück | 3400 g Banane
4: Freitag | Frühstück | 3400 g Apfel
4: Freitag | Frühstück | 850 g Cherry-Tomaten
4: Freitag | Frühstück | 2550 g Gurken
4: Freitag | Frühstück | 68 Brötchen, weiß
4: Freitag | Frühstück | 229.5 Brötchen, Körner
4: Freitag | Frühstück | 3.54 kg Große Dose Bohnen
4: Freitag | Frühstück | 11.33 l Vollei
4: Freitag | Abendessen | 60 g Erbesenprotein-Schnetzel
4: Freitag | Abendessen | 20 kg Wadschunken (Hesse/Beinscheibe) vom Rind
4: Freitag | Abendessen | 1000 g Schweineschmalz
4: Freitag | Abendessen | 1000 ml Rapsöl
4: Freitag | Abendessen | 20 kg Zwiebeln
4: Freitag | Abendessen | 100 g Zucker
4: Freitag | Abendessen | 6 l Bier, dunkles
4: Freitag | Abendessen | 300 g Kümmel, ganz
4: Freitag | Abendessen | 300 g Majoran, getrocknet, gerebelt
4: Freitag | Abendessen | 800 g Paprikapulver, edelsüß
4: Freitag | Abendessen | 400 g Paprikapulver, rosenscharf
4: Freitag | Abendessen | 300 ml Branntweinessig
4: Freitag | Abendessen | 6000 g Paprika mix
4: Freitag | Abendessen | 6400 g Kartoffeln frisch
4: Freitag | Abendessen | 6400 ml passierte Tomaten
4: Freitag | Abendessen | 6400 ml Tomaten in Stücken
4: Freitag | Abendessen | 1600 ml Rotwein (vegan)
4: Freitag | Abendessen | 2000 g Karotten
4: Freitag | Abendessen | 1600 ml Gemüsebrühe
4: Freitag | Abendessen | 80 g Paprika edelsüss
4: Freitag | Abendessen | 40 g Paprika rosenscharf
4: Freitag | Abendessen | 16 g Pfeffer schwarz
4: Freitag | Abendessen | 200 g Knoblauch
4: Freitag | Abendessen | 2000 g Zwiebeln
4: Freitag | Abendessen | Majoran
4: Freitag | Abendessen | Thymian
4: Freitag | Abendessen | Oregano
4: Freitag | Abendessen | Sojasauce
4: Freitag | Abendessen | Zucker
4: Freitag | Abendessen | 3200 g Sojaschnetzel
4: Freitag | Abendessen | 600 ml CHEF Flüssiges Konzentrat Vegan Like Beef
4: Freitag | Abendessen | 1000 ml Rapsöl
4: Freitag | Abendessen | 800 g Tomatenmark
4: Freitag | Abendessen | 20000 g Pasta
4: Freitag | Abendessen | 8000 g Milchreis
4: Freitag | Abendessen | 32 l Haferdrink, ca. 1.8% Fett
4: Freitag | Abendessen | 10.67 Zitrone, Zeste davon
4: Freitag | Abendessen | 768 g Zucker
4: Freitag | Abendessen | Salz
4: Freitag | Abendessen | 3200 g Fruchtcocktail (Abtropfgewicht)
4: Freitag | Abendessen | 1600 g Apfelkompott
4: Freitag | Abendessen | 3200 g Schattenmorellen
4: Freitag | Abendessen | 1600 g Zucker
5: Samstag | Frühstück | 2550 g Alsan
5: Samstag | Frühstück | 850 g Frischkäse (gemischte Auswahl)
5: Samstag | Frühstück | 1275 g Veganer Aufstrich (Gläser)
5: Samstag | Frühstück | 127.5 Scheiben Käse
5: Samstag | Frühstück | 42.5 Scheiben Veganer Käse
5: Samstag | Frühstück | 85 Scheiben Wurst
5: Samstag | Frühstück | 85 Scheiben vegane Wurstalternative (z.B. Rügenwalder)
5: Samstag | Frühstück | 425 g Fleischsalat
5: Samstag | Frühstück | 3400 g Müsli, diverse
5: Samstag | Frühstück | 2550 g Joghurt
5: Samstag | Frühstück | 850 g Sojajoghurt (ungesüßt)
5: Samstag | Frühstück | 510 g Erdnussbutter, creamy
5: Samstag | Frühstück | 510 g Erdnussbutter, crunchy
5: Samstag | Frühstück | 2550 g Nutella
5: Samstag | Frühstück | 1700 g Marmelade
5: Samstag | Frühstück | 510 g Pflaumenmus
5: Samstag | Frühstück | 850 g Honig
5: Samstag | Frühstück | 34 Früchtetee-Beutel (gemischt)
5: Samstag | Frühstück | 34 Kräutertee-Beutel
5: Samstag | Frühstück | 34 Schwarz-/Grüntee-Beutel
5: Samstag | Frühstück | 4250 g Kaffeepulver
5: Samstag | Frühstück | 510 g Kakaopulver
5: Samstag | Frühstück | 1700 g Zucker
5: Samstag | Frühstück | 42.5 Stk. Süßstoff-Tabletten
5: Samstag | Frühstück | 12750 ml Milch
5: Samstag | Frühstück | 8500 ml Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
5: Samstag | Frühstück | 21250 ml Orangensaft
5: Samstag | Frühstück | 3400 g Banane
5: Samstag | Frühstück | 3400 g Apfel
5: Samstag | Frühstück | 850 g Cherry-Tomaten
5: Samstag | Frühstück | 2550 g Gurken
5: Samstag | Frühstück | 68 Brötchen, weiß
5: Samstag | Frühstück | 229.5 Brötchen, Körner
6: Sonntag | Abbau-Frühstück | 300 g Alsan
6: Sonntag | Abbau-Frühstück | 150 g Veganer Aufstrich (Gläser)
6: Sonntag | Abbau-Frühstück | 30 Scheiben Käse
6: Sonntag | Abbau-Frühstück | 20 Scheiben vegane Wurstalternative (z.B. Rügenwalder)
6: Sonntag | Abbau-Frühstück | 300 g Nutella
6: Sonntag | Abbau-Frühstück | 200 g Marmelade
6: Sonntag | Abbau-Frühstück | 500 g Kaffeepulver
6: Sonntag | Abbau-Frühstück | 200 g Zucker
6: Sonntag | Abbau-Frühstück | 500 ml Milch
6: Sonntag | Abbau-Frühstück | 500 ml Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
6: Sonntag | Abbau-Frühstück | 100 g Gurken
6: Sonntag | Abbau-Frühstück | 8 Brötchen, weiß
6: Sonntag | Abbau-Frühstück | 27 Brötchen, Körner
