# Vegane "Käse"-Lauch-"Hack"-Suppe

*vegan, vegetarisch, herzhaft*

**6 Portionen**

---

- *150 g* Zwiebeln
- *5 Stk* Lauch
- *3 Zeh* Knoblauch 
- *750 g* Rügenwalder Mühlen-Hack
- *1 l* Sojamilch
- *400 ml* Sojasahne
- *150 g* Vegane Creme Fraiche (Creme Vega o.Ä.)
- *3 EL* Misopaste 
- *1.5 TL* Milchsäurepulver
- *4 EL* Hefeflocken 
- *3 TL* Natriumcitrat
- *1/4* Muskatnuss
- *2 TL* Pfeffer 
- Gemüsebrühepulver
- Salz


---

- Zwiebeln würfeln
- Lauch in halbe Ringe schneiden und gründlich Waschen
- Zwiebeln und Lauch in einer Pfanne anbraten (falls nötig portionsweise), in einem Topf geben
- Hack ebenfalls anbraten und mit in den Topf geben
- Sojamilch, Sojasahne und Creme Fraiche dazugeben, unterrühren und aufkochen
- Misopaste und Milchsäurepulver mit etwas Wasser glattrühren um Klumpenbildung zu vermeiden, mit in den Topf kippen
- Hefeflocken und Natriumcitrat unterrühren
- 10 Minuten köcheln lassen
- Mit Muskatnuss, Pfeffer, Gemüsebrühe und Salz würzen