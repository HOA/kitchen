# HOA 2023

---


## -1: Sonntag

### Aufbau-Frühstück

- *30 Personen* [Aufbaufrühstück](./Fruehstueck/Aufbaufruestueck.md)

### Abendessen

- Frittierzeug zum Abendessen für Sonntag



## 0: Montag

### Aufbau-Frühstück

- *50 Personen* [Aufbaufrühstück](./Fruehstueck/Aufbaufruestueck.md)

### Abendessen

- *25 Personen* [Tomatensauce](./Tortellini/Tomatensauce.md)
- *25 Personen* [Sahnesauce](./Tortellini/Sahnesauce.md)
- *7.5 kg* Vegetarische Tortelloni
- *2.5 kg* Vegane Tortelloni



## 1: Dienstag

### Aufbau-Frühstück

- *60 Personen* [Aufbaufrühstück](./Fruehstueck/Aufbaufruestueck.md)

### Abendessen

- *80 Personen* [Veganes Butter Chicken](./Butter_Chicken/Veganes_Butter_Chicken.md)
- *80 Personen* [Butter Chicken](./Butter_Chicken/Butter_Chicken.md)
- *100 g* Pflanzenjoghurt ohne Soja
- *60 g* Erbesenprotein-Schnetzel
- *20 kg* Basmatireis
- *160 Personen* [Rote Grütze](./Rote_Gruetze.md)


## 2: Mittwoch

### Frühstück

- *170 Personen* [Frühstück](./Fruehstueck/Fruehstueck.md)

### Abendessen

- *80 Portionen* [Soljanka Vegan](./Soljanka/Soljanka_Vegan.md)
- *80 Portionen* [Soljanka mit Fleisch](./Soljanka/Soljanka_mit_Fleisch.md)
- *160 Personen* [Grüner Beilagensalat](./Gruener_Beilagensalat.md)



## 3: Donnerstag

### Frühstück

- *170 Personen* [Frühstück](./Fruehstueck/Fruehstueck.md)
- *170 Personen* [Kaiserschmarrn](./Fruehstueck/Kaiserschmarrn_vegan.md)

### Abendessen

- *160 Portionen* Burger (50% Vegan)
- *160 Personen* [Coleslaw](./Coleslaw.md)


## 4: Freitag

### Frühstück

- *170 Personen* [Frühstück](./Fruehstueck/Fruehstueck.md)
- *170 Personen* [Baked Beans mit Rührei](./Fruehstueck/Baked-Beans-und-Ruehrei.md)

### Abendessen

- *80 Personen* [Wiener Wirtshausgulasch](./Gulasch/Wiener_Wirtshausgulasch.md)
- *80 Personen* [Veganes Gulasch](./Gulasch/Veganes_Gulasch.md)
- *60 g* Erbesenprotein-Schnetzel
- *160 Personen* [Nudelbeilage](./Gulasch/Nudelbeilage.md)
- *160 Portionen* [Hafer Milchreis mit Zitrone](./Hafer_Milchreis_mit_Zitrone.md)



## 5: Samstag

### Frühstück

- *170 Personen* [Frühstück](./Fruehstueck/Fruehstueck.md)



## 6: Sonntag

### Abbau-Frühstück

- *20 Personen* [Aufbaufrühstück](./Fruehstueck/Aufbaufruestueck.md)