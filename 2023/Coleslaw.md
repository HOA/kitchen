# Coleslaw

**12 Personen**

---

- *2 kg* Weißkohl
- *125 g* Karotten
- *100 g* rote Zwiebel
- *15 g* Petersilie
- *200 g* Zucker
- *140 g* Salz

## Dressing

- *200 g* vegane Mayonnaise
- *70 ml* Apfelessig
- *30 g* Senf
- *1 EL* Pfeffer, frisch gemahlen
- *35 g* Zucker

--- 


1. Für den Slaw-Mix: Kohl, Zwiebel, Karotte und Petersilie in einer großen Schüssel vermengen und genug Platz zum Umrühren lassen. Mit Zucker und Salz bestreuen und vermengen. 5 Minuten ruhen lassen, dann in ein großes Sieb geben und gründlich unter kaltem, fließendem Wasser abspülen.

2. Die gespülte Mischung in einen Salatschleuder geben und trocken schleudern. Alternativ auf ein großes Backblech mit hohem Rand legen, das mit drei Lagen Küchenpapier oder einem sauberen Küchentuch ausgelegt ist, und die Mischung mit weiteren Küchenpapieren trocken tupfen. Zurück in die große Schüssel geben und beiseite stellen.


3. Für das Dressing: Mayonnaise, Essig, Senf, schwarzer Pfeffer und Zucker in einer mittelgroßen Schüssel vermengen und rühren, bis eine homogene Mischung entsteht.

4. Das Dressing über die Kohlmischung gießen und umrühren, um alles gut zu überziehen. Abschmecken und bei Bedarf mit mehr Salz, Pfeffer, Zucker und/oder Essig nachwürzen.