# HOA 2022 (Einkauf 2)

---

## Tag 3 (Donnerstag)

- *60 Personen* Linsen und Spätzle mit Saitenwürstle
- *60 Personen* Linsen und Spätzle mit Räuchertofu

## Tag 4 (Freitag)

- *120 Personen* [Frühstück](Fruehstueck.md)
- *60 Personen* [Wiener Wirtshausgulasch](./Wiener_Wirtshausgulasch.md)
- *60 Personen* [Nudelbeilage](./Nudelbeilage.md)
- *60 Personen* [Cremiges Erdäpfelgulasch](./Cremiges_Erdaepfelgulasch.md)

## Tag 5 (Samstag)

- *120 Personen* [Frühstück](Fruehstueck.md)

## Tag 6 (Sonntag)

- *40 Personen* [Frühstück](Fruehstueck.md)

## Frittiertes

- TODO