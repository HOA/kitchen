# HOA 2022 (Einkauf 1)

---

## Tag 0 (Montag)

- *50 Personen* [Aufbau-Frühstück](Fruehstueck.md)
- *60 Personen* [Asia-Nudeln](Asia_Nudeln.md)

## Tag 1 (Dienstag)

- *120 Personen* [Frühstück](Fruehstueck.md)
- *60 Portionen* [Soljanka Vegan](./Soljanka_Vegan.md)
- *60 Portionen* [Soljanka mit Fleisch](./Soljanka_mit_Fleisch.md)

## Tag 2 (Mittwoch)

- *120 Personen* [Frühstück](Fruehstueck.md)
- *100 Personen* [Kicherbsen-Curry](Kichererbsen_Curry.md)

## Tag 3 (Donnerstag)

- *120 Personen* [Frühstück](Fruehstueck.md)
- *40 Portionen* [Milchreis-mit-Zitrone](./Milchreis-mit-Zitrone.md)
- *20 Portionen* [Hafer-Milchreis-mit-Zitrone](./Hafer-Milchreis-mit-Zitrone.md)
- Trockenzutaten für Linsen & Spätzle

## Vorbestellen

- Vorbestellen: Beinscheiben für Gulasch
- Vorbestellen: Räuchertofu für Linsen & Spätzle

## Frittiertes

- *40 kg* Steakhouse-Fries (Portion ~= 200g)
- *50* Currywürste 
- *21 kg* XL-Nuggets (Portion = 5 Stk.)
- Frühlingsrollen
- Soßen für Frittiertes

## Misc

- *100 Waffeln* [WOC-Waffeln](./WOC_Waffeln.md)
- *2 kg* Salz