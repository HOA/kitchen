# Frühstück

Frühstücksdinge.

**20 Personen**

---

- *300 g* Butter
- *200 g* Margarine (vegan!)
- *100 g* Frischkäse (gemischte Auswahl)
- *100 g* Veganer Aufstrich

- *20 Scheiben* Käse
- *20 Scheiben* Wurst
- *100 g* Fleischsalat

- *400 g* Müsli, diverse
- *300 g* Joghurt
- *100 g* Sojajoghurt (ungesüßt)

- *60 g* Erdnussbutter, creamy
- *60 g* Erdnussbutter, crunchy
- *300 g* Nutella
- *200 g* Marmelade
- *60 g* Pflaumenmus
- *100 g* Honig

- *4* Früchtetee-Beutel (gemischt)
- *4* Kräutertee-Beutel
- *4* Schwarz-/Grüntee-Beutel
- *500 g* Kaffeepulver
- *60 g* Kakaopulver
- *200 g* Zucker
- *5 Stk.* Süßstoff-Tabletten

- *2000 ml* Milch
- *1000 ml* Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
- *2000 ml* Orangensaft

- *400 g* Banane
- *400 g* Apfel

- *300 g* Gurken

- *8* Brötchen, weiß
- *27* Brötchen, Körner
- *1/2* Körnerbrot