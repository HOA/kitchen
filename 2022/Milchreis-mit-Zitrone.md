# Milchreis mit Zitrone

*Dessert, Vegetarisch*

**5 Portionen**

----

## Milchreis

- *250 g* Milchreis
- *1 l* Milch, 3.5%
- *1/3* Zitrone, Zeste davon
- *24 g* Zucker
- Salz

## Beilage

- *100 g* Fruchtcocktail (Abtropfgewicht)
- *50 g* Apfelkompott
- *100 g* Schattenmorellen
- *50 g* Zucker

----

Milch, Reis und Zucker in einen Topf geben und zum Kochen bringen.
Währenddessen Zitronenzeste herstellen und dazu geben.

Nach dem Kochen ca. 25 .. 35 Minuten ziehen lassen.

Beilage dazu servieren.
