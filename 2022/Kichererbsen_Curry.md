# Kichererbsen-Curry

**4 Personen**

---

## Joghurt-Topping

- *1/2* Salatgurke
- *1/6 Bund* Koriander
- *1/6 Bund* glatte Petersilie
- *250 g* Joghurt (75% Naturjoghurt, 25% Sojajoghurt)

# Reis

- *300 g* Basmatireis 

# Curry

- *4* Knoblauchzehen
- *250 g* Zwiebeln
- *5 g* Ingwer
- *2 EL* Bratöl 
- *1/2 TL* Kreuzkümmel-Samen
- *2* Lorbeerblätter
- *400 g* Dosen-Tomaten (stückig)
- *800 g* Kichererbsen (Dose, Abtropfgewicht)
- *150 ml* Wasser
- *2 TL* Garam Masala
- *1/2 TL* Kurkuma
- *2 EL* Zitronensaft 

---

1. Gurke waschen und grob raspeln. Joghurt mit Gurken verrühren und kräftig mit
   Salz abschmecken. Kalt stellen.
2. Koriander und Petersilie kalt abbrausen, trocken schütteln und Blättchen
   abzupfen. Knoblauch, Zwiebeln schälen und Ingwer schälen und fein hacken.
3. Kichererbsen in Sieb abspülen und gut abtropfen lassen,
4. Öl in Topf bei kleiner Hitze erwärmen, Kreuzkümmel-Samen und Lorbeerblätter
   zugegeben ca. ½ Minute bei kleiner Hitze anbraten. 
5. Knoblauch, Zwiebeln und Ingwer zugeben und ca. 5 Minuten unter gelegentlichem
   Rühren anbraten. Tomaten zugeben und ca. 6 Minuten garen.
6. Kichererbsen mit Garam Masala, Kurkuma und Wasser zu den Tomaten geben. Mit
   Salz würzen und 10 Minuten bei kleiner Hitze im geschlossenen Topf köcheln
   lassen.
7. Mit Zitronensaft und Salz abschmecken und Lorbeerblätter ggf. herausnehmen.
   Curry mit Reis, Joghurt und Kräutern servieren.
