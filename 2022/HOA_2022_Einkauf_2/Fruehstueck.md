# Frühstück

Frühstücksdinge.

**40 Personen**

---

- *600 g* Butter
- *400 g* Margarine (vegan!)
- *200 g* Frischkäse (gemischte Auswahl)
- *200 g* Veganer Aufstrich
- *40 Scheiben* Käse
- *40 Scheiben* Wurst
- *200 g* Fleischsalat
- *800 g* Müsli, diverse
- *600 g* Joghurt
- *200 g* Sojajoghurt (ungesüßt)
- *120 g* Erdnussbutter, creamy
- *120 g* Erdnussbutter, crunchy
- *600 g* Nutella
- *400 g* Marmelade
- *120 g* Pflaumenmus
- *200 g* Honig
- *8* Früchtetee-Beutel (gemischt)
- *8* Kräutertee-Beutel
- *8* Schwarz-/Grüntee-Beutel
- *1000 g* Kaffeepulver
- *120 g* Kakaopulver
- *400 g* Zucker
- *10 Stk.* Süßstoff-Tabletten
- *4000 ml* Milch
- *2000 ml* Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
- *4000 ml* Orangensaft
- *800 g* Banane
- *800 g* Apfel
- *600 g* Gurken
- *16* Brötchen, weiß
- *54* Brötchen, Körner
- *1* Körnerbrot