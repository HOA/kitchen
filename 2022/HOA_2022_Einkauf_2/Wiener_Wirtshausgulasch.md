# Wiener Wirtshausgulasch

Als Stärkebeilage traditionell Semmeln, alternativ geht aber auch Kartoffeln, Nudeln etc.

*beef, stew, fall, winter*

**60 Personen**

---

- *15 kg* Wadschunken (Hesse/Beinscheibe) vom Rind
- *750 g* Schweineschmalz
- *750 ml* Rapsöl
- *15 kg* Zwiebeln
- *75 g* Zucker
- *4.5 l* Bier, dunkles
- *225 g* Kümmel, ganz
- *225 g* Majoran, getrocknet, gerebelt
- *600 g* Paprikapulver, edelsüß
- *300 g* Paprikapulver, rosenscharf
- *225 ml* Apfelessig

---

- Wadschunken in ca 4x4cm große Würfel schneiden 
- Zwiebeln mittelgrob würfeln (1-2 cm Kantenlänge)
- In Schweineschmalz in einzelner Lage scharf anbraten (nach Bedarf portionsweise)
- Fleisch beiseite stellen
- Zwiebeln in gleicher Pfanne anbraten bis sie goldbraun sind, am Ende der Bratzeit Zucker dazugeben
- Zwiebeln mit Fleisch, Fleischsaft und Bier in Topf geben und schmoren
- Gewürze zugeben, nach Bedarf mit Wasser aufgießen, dass das Fleisch zu zwei Drittel mit Flüssigkeit bedeckt ist

Quelle: [Frau Ziii](https://www.ziiikocht.at/2013/01/wiener-wirtshausgulasch-aus-dem.html)