# Milchreis mit Zitrone

*Dessert, Vegetarisch*

**40 Portionen**

---

## Milchreis

- *2000 g* Milchreis
- *8 l* Milch, 3.5%
- *2.67* Zitrone, Zeste davon
- *192 g* Zucker
- Salz

## Beilage

- *800 g* Fruchtcocktail (Abtropfgewicht)
- *400 g* Apfelkompott
- *800 g* Schattenmorellen
- *400 g* Zucker

---

Milch, Reis und Zucker in einen Topf geben und zum Kochen bringen.
Währenddessen Zitronenzeste herstellen und dazu geben.

Nach dem Kochen ca. 25 .. 35 Minuten ziehen lassen.

Beilage dazu servieren.