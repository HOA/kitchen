# Hafermilchreis mit Zitrone

*Dessert, Vegetarisch*

**20 Portionen**

---

## Milchreis

- *1000 g* Milchreis
- *4 l* Haferdrink, ca. 1.8% Fett
- *1.33* Zitrone, Zeste davon
- *96 g* Zucker
- Salz

## Beilage

- *400 g* Fruchtcocktail (Abtropfgewicht)
- *200 g* Apfelkompott
- *400 g* Schattenmorellen
- *200 g* Zucker

---

Haferdrink, Reis und Zucker in einen Topf geben und zum Kochen bringen.
Währenddessen Zitronenzeste herstellen und dazu geben.

Nach dem Kochen ca. 25 .. 35 Minuten ziehen lassen.

Beilage dazu servieren.