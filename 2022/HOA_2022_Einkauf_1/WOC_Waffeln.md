# WOC-Waffeln

**100 Waffeln**

---

- *2 l* Vollei
- *2 kg* Zucker
- *2.5 kg* Butter
- *200 g* Vanillin-Zucker
- *100 g* Backpulver
- *2 l* Milch
- *4 kg* Mehl
- *1 l* Club-Mate
- *100 g* Salz