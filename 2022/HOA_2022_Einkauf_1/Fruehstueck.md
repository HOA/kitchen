# Frühstück

Frühstücksdinge.

**120 Personen**

---

- *1800 g* Butter
- *1200 g* Margarine (vegan!)
- *600 g* Frischkäse (gemischte Auswahl)
- *600 g* Veganer Aufstrich
- *120 Scheiben* Käse
- *120 Scheiben* Wurst
- *600 g* Fleischsalat
- *2400 g* Müsli, diverse
- *1800 g* Joghurt
- *600 g* Sojajoghurt (ungesüßt)
- *360 g* Erdnussbutter, creamy
- *360 g* Erdnussbutter, crunchy
- *1800 g* Nutella
- *1200 g* Marmelade
- *360 g* Pflaumenmus
- *600 g* Honig
- *24* Früchtetee-Beutel (gemischt)
- *24* Kräutertee-Beutel
- *24* Schwarz-/Grüntee-Beutel
- *3000 g* Kaffeepulver
- *360 g* Kakaopulver
- *1200 g* Zucker
- *30 Stk.* Süßstoff-Tabletten
- *12000 ml* Milch
- *6000 ml* Milchersatz, vegan (Reis-/Dinkel-/Hafer-/Sojamilch)
- *12000 ml* Orangensaft
- *2400 g* Banane
- *2400 g* Apfel
- *1800 g* Gurken
- *48* Brötchen, weiß
- *162* Brötchen, Körner
- *3* Körnerbrot