# Kichererbsen-Curry

**100 Personen**

---

## Joghurt-Topping

- *12.5* Salatgurke
- *4.17 Bund* Koriander
- *4.17 Bund* glatte Petersilie
- *6250 g* Joghurt (75% Naturjoghurt, 25% Sojajoghurt)

## Reis

- *7500 g* Basmatireis

## Curry

- *100* Knoblauchzehen
- *6250 g* Zwiebeln
- *125 g* Ingwer
- *50 EL* Bratöl
- *12.5 TL* Kreuzkümmel-Samen
- *50* Lorbeerblätter
- *10000 g* Dosen-Tomaten (stückig)
- *20000 g* Kichererbsen (Dose, Abtropfgewicht)
- *3750 ml* Wasser
- *50 TL* Garam Masala
- *12.5 TL* Kurkuma
- *50 EL* Zitronensaft

---

1. Gurke waschen und grob raspeln. Joghurt mit Gurken verrühren und kräftig mit
   Salz abschmecken. Kalt stellen.
2. Koriander und Petersilie kalt abbrausen, trocken schütteln und Blättchen
   abzupfen. Knoblauch, Zwiebeln schälen und Ingwer schälen und fein hacken.
3. Kichererbsen in Sieb abspülen und gut abtropfen lassen,
4. Öl in Topf bei kleiner Hitze erwärmen, Kreuzkümmel-Samen und Lorbeerblätter
   zugegeben ca. ½ Minute bei kleiner Hitze anbraten. 
5. Knoblauch, Zwiebeln und Ingwer zugeben und ca. 5 Minuten unter gelegentlichem
   Rühren anbraten. Tomaten zugeben und ca. 6 Minuten garen.
6. Kichererbsen mit Garam Masala, Kurkuma und Wasser zu den Tomaten geben. Mit
   Salz würzen und 10 Minuten bei kleiner Hitze im geschlossenen Topf köcheln
   lassen.
7. Mit Zitronensaft und Salz abschmecken und Lorbeerblätter ggf. herausnehmen.
   Curry mit Reis, Joghurt und Kräutern servieren.