# Asia-Nudeln

**60 Personen**

---

- *6250 g* Mie-Nudeln, ohne Ei
- *4000 g* Pilze
- *3750 g* Möhren
- *2000 g* Mungobohnensprossen
- *400 g* Ingwer
- *20 Bund* Frühlingszwiebeln
- *1500 ml* Rapsöl
- *5* Blumenkohl
- *5* Brokkoli

## Soße

- *750 ml* Hoisin-Soße
- *200 ml* Teriaky-Soße
- *1250 g* Mangochutney
- *100 ml* Sesamöl
- *150 g* Tamarinden-Paste
- *1000 ml* Gemüsebrühe

---

- Brokkoli und Blumenkohl in Röschen zerteilen und blanchieren
- Pilze vierteln
- Möhren in Streifen schneiden
- Frühlingszwiebeln in Ringe schneiden, dabei weißen und grünen Teil trennen
- Ingwer fein hacken
- Sprossen waschen
- Soßen-Zutaten vermischen und bereit stellen
- Nudeln nach Verpackungsanweisung zubereiten
- Möhren anbraten, dann Ingwer und weißen Teil der Frühlingszwiebeln zugeben und
  mitbraten
- Pilze zugeben und kurz mitbraten
- Blumenkohl, Brokkoli, Sprossen und Nudeln zugeben und untermischen
- Soße zugeben und kurz braten
- Grünen Teil der Frühlingszwiebeln untermischen und servieren