# Soljanka (Vegan)

Angepasst von [Vegane Soljanka mit Würstchen](https://veganuschka.de/rezepte/suppen/vegane-soljanka-mit-wuerstchen/)

*15 min vorbereitung, 50 min gesamt*

**60 Portionen**

---

## Eintopf

- *2250 g* Möhren
- *2325 g* Paprika, rot
- *4500 g* Kartoffeln
- *3300 g* Zwiebeln
- *3000 g* Tofu
- *450 g* Tomatenmark
- *210 g* Knoblauch
- *450 ml* Öl
- *3 g* Lorbeerblatt
- *7.5* Bio-Zitrone
- *7500 ml* Wasser
- *105 g* Pfeffer
- *120 g* Paprikapulver, edelsüß
- *60 g* Zucker
- *120 g* Gemüsebrühe

## Garnitur

- *1800 g* Gewürzengurken
- *3000 g* (Veganer) Frischkäse oder Crème fraîche
- *150 g* Schnittlauch
- *150 g* Petersilie

---

Zunächst das Gemüse vorbereiten: Kartoffeln schälen und würfeln. Zwiebeln und Möhren in kleine Würfel schneiden und den Knoblauch kleinhacken. Die Paprika zerteilen und in in mundgroße Stücke würfeln. Den Tofu in Steifen schneiden. Die Gurken abtropfen und in Scheiben schneiden. Schnittlauch und Petersilie kleinhacken.

Die Kartoffelwürfel ins kalte Wasser geben. Gemüsebrühe sowie ein Lorbeerblatt hinzufügen und aufkochen. Kartoffeln halb weich garen.

Öl in einer Pfanne erhitzen. Zwiebeln und Möhren kurz darin anbraten. Knoblauch dazu geben und ca. 1 Minute mitanschwitzen. Tomatenmark, Paprikapulver hinzufügen und weitere 5 Minuten bei kleiner Hitze anbraten. Das angebratene Gemüse zu den kochenden Kartoffelwürfeln geben.

Die Paprika und den Tofu kurz scharf anbraten und mit zu den Kartoffelwürfeln geben und 15 Minuten köcheln lassen.

Mit Zucker, Pfeffer, Salz, Zitronensaft, abschmecken und kurz aufkochen. Serviert wird die Soljanka mit veganem Frischkäse oder Crème fraîche.