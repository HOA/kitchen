# Asia-Nudeln

**12 Personen**

---

- *1250 g* Mie-Nudeln, ohne Ei
- *800 g* Pilze
- *750 g* Möhren
- *400 g* Mungobohnensprossen
- *80 g* Ingwer
- *4 Bund* Frühlingszwiebeln
- *300 ml* Rapsöl
- *1* Blumenkohl
- *1* Brokkoli

## Soße

- *150 ml* Hoisin-Soße
- *40 ml* Teriaky-Soße
- *250 g* Mangochutney
- *20 ml* Sesamöl
- *30 g* Tamarinden-Paste
- *200 ml* Gemüsebrühe

---

- Brokkoli und Blumenkohl in Röschen zerteilen und blanchieren
- Pilze vierteln
- Möhren in Streifen schneiden
- Frühlingszwiebeln in Ringe schneiden, dabei weißen und grünen Teil trennen
- Ingwer fein hacken
- Sprossen waschen
- Soßen-Zutaten vermischen und bereit stellen
- Nudeln nach Verpackungsanweisung zubereiten
- Möhren anbraten, dann Ingwer und weißen Teil der Frühlingszwiebeln zugeben und
  mitbraten
- Pilze zugeben und kurz mitbraten
- Blumenkohl, Brokkoli, Sprossen und Nudeln zugeben und untermischen
- Soße zugeben und kurz braten
- Grünen Teil der Frühlingszwiebeln untermischen und servieren