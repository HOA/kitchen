# Veganes Butter Chicken

**10 Personen**

---

## Makhani Sauce

### Gewürzmischung

- *24 g* Bockshornklee, gemahlen
- *0.75 g* Nelke, gemahlen
- *1 g* Kardamom, gemahlen
- *15 g* Garam Masala (glutenfrei!)
- *3 g* Salz

### Sauce

- *150 g* Zwiebeln
- *1.5 g* Backnatron
- *15 g* Ingwer
- *12 g* Knoblauch
- *30 g* Cashews, ungeröstet, ungesalzen
- *250 ml* Wasser
- *800 g* geschälte Tomaten (Konserve)
- *120 ml* Sojasahne (Glutenfrei)
- *60 g* vegane Butter (Glutenfrei)

## Marinade

- *150 g* Sojajoghurt
- *8 g* Bockshornklee, gemahlen
- *20 g* Garam Masala (glutenfrei!)
- *8 g* Ingwer
- *5 g* Salz
- *6 g* Pfeffer, gemahlen

## Hähnchen

- *400 g* Sojaschnetzel, mittelgrob
- *100 ml* CHEF Flüssiges Konzentrat Vegan Like Chicken (https://www.metro.de/marktplatz/product/4d2fd05b-7aa6-4573-8225-2e4924b9f541)

## Beilage

- *1 kg* Basmatireis

---

## Vorbereitung

- Zwiebeln grob würfeln
- Cashews in etwas Wasser aufkochen
- Knoblauch fein hacken
- Ingwer für Marinade und Sauce getrennt fein hacken
- Mit Konzentrat heiße Brühe anrühren, Schnetzel darin aufweichen, dann auspressen

## "Chicken"

- Marinade-Zutaten vermischen
- Ausgepresste Sojaschnetzel hineingeben & marinieren
- Backen oder portionsweise anbraten und beiseite stellen

## Sauce

- Zwiebeln mit Backpulver anbraten, bis komplett zerfallen
- Nach Bedarf mit Wasser ablöschen um anbrennen zu verhindern
- Knoblauch & Ingwer dazugeben und kurz mitbraten
- Gewürzmischung dazugeben und unterrühren
- Cashews samt Flüssigkeit dazugeben
- Tomaten dazugeben
- Aufkochen und für ca. 45 min köcheln lassen
- Pürieren bis die Sauce glatt ist
- Butter und Sahne zugeben und unterpürieren
- Nach Bedarf mit Wasser die Konsistenz justieren
- Sojaschnetzel dazugeben
- Mit Reis servieren