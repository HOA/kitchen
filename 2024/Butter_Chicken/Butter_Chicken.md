# Butter Chicken

**10 Personen**

---

## Makhani Sauce

### Gewürzmischung

- *24 g* Bockshornklee, gemahlen
- *0.75 g* Nelke, gemahlen
- *1 g* Kardamom, gemahlen
- *15 g* Garam Masala (glutenfrei!)
- *3 g* Salz

### Sauce

- *150 g* Zwiebeln
- *1.5 g* Backnatron
- *15 g* Ingwer
- *12 g* Knoblauch
- *30 g* Cashews, ungeröstet, ungesalzen
- *800 g* geschälte Tomaten (Konserve)
- *250 ml* Wasser
- *120 ml* Sahne
- *60 g* Butter

## Marinade

- *100 g* Griechischer Joghurt
- *8 g* Bockshornklee, gemahlen
- *20 g* Garam Masala (glutenfrei!)
- *8 g* Ingwer
- *10 g* Salz
- *6 g* Pfeffer, gemahlen

## Hähnchen

- *1500 g* Hähnchenoberkeule ohne Haut, ohne Knochen

## Beilage

- *1 kg* Basmatireis

---


## Vorbereitung

- Zwiebeln grob würfeln
- Cashews in etwas Wasser aufkochen
- Knoblauch fein hacken
- Ingwer für Marinade und Sauce getrennt fein hacken
- Hähnchen in grobe Würfel zerlegen

## "Chicken"

- Marinade-Zutaten vermischen
- Hähnchen hineingeben & marinieren
- Backen oder portionsweise anbraten und beiseite stellen

## Sauce

- Zwiebeln mit Backpulver anbraten, bis komplett zerfallen
- Nach Bedarf mit Wasser ablöschen um anbrennen zu verhindern
- Knoblauch & Ingwer dazugeben und kurz mitbraten
- Gewürzmischung dazugeben und unterrühren
- Cashews samt Flüssigkeit dazugeben
- Tomaten dazugeben
- Aufkochen und für ca. 45 min köcheln lassen
- Pürieren bis die Sauce glatt ist
- Butter und Sahne zugeben und unterpürieren
- Nach Bedarf mit Wasser die Konsistenz justieren
- Hähnchen dazugeben
- Mit Reis servieren