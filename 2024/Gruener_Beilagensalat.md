# Grüner Beilagensalat

**4 Personen**

---

- *200 g* Gemischter Salat, geschnitten & gewaschen

## Dressing

- *5 g* Senf, mittelscharf
- *50 ml* Weißweinessig
- *100 ml* Neutrales Rapsöl
- *25 g* Italienische Kräuter (TK) 
- *1.5 g* Knoblauch
- *5 g* Gemüsebrühe (glutenfrei, sojafrei!)
- *3 g* Pfeffer
- *10 g* Zucker

