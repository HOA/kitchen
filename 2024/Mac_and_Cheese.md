# Mac and Cheese

**10 Portionen (als Beilage)**

---

- *625 g* Maccheroni
- *5 EL* Butter
- *5 EL* Mehl
- *1000 ml* Milch
- *500 g* Käse, gerieben
- Cayennepfeffer
- Salz
- Pfeffer

---

- Butter in einem Topf zerlassen.

- Die Milch nach und nach hinzugeben, dabei weiter rühren, damit sich keine
  Klümpchen bilden. Die Gewürze hinzugeben und die Sauce einige Minuten köcheln
  lassen, bis sie cremig ist.

- Das Mehl in die flüssige Butter streuen. Mit dem Schneebesen etwa eine Minute
  unter ständigem Rühren bräunen lassen.

- Die Hälfte des Käses unterrühren und schmelzen lassen. Die Sauce abschmecken
  und warm halten.

- Ofen auf 180 Grad vorheizen.  

- Die Nudeln in Salzwasser kochen, bis sie schon etwas weich, aber noch nicht
  ganz gar sind. Nudeln in Auflaufform verteilen.

- Soße zu den Nudeln geben.

- Die Masse in eine Auflaufform geben, den restlichen Käse darauf verstreuen und
  für etwa 20 Minuten backen, bis der Käse gebräunt ist.
