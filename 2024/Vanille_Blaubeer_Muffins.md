# Vanille-Blaubeer-Muffins

**6 Personen**

---

- *80g* Butter
- *80g* Zucker
- *60g* Vollei
- *180g* Naturjoghurt
- *210g* Mehl (450)
- *9g* Backpulver
- *2g* Salz
- *20g* Vanillepuddingpulver
- *110g* gefrorene Blaubeeren
- *7* Papier-Backformen "Muffin" (groß)

---

* 180°C Umluft, 25 Minuten backen
