# Vegane Vanille-Blaubeer-Muffins

**36 Muffins**

---

- *480g* vegane Butter
- *360g* Zucker
- *6* Eier-Eiersatz (mit 180ml Flüssigkeit im Fertigprodukt)
- *1080g* Sojajoghurt (ungesüßt)
- *1260g* Mehl (450)
- *54g* Backpulver
- *48g* veganer Vanillepuddingpulver
- *12g* Salz
- *660g* gefrorene Blaubeeren
- *4EL* O-Saft
- *7* Papier-Backformen "Muffin" (groß)

---

## Vorbereitung
* Butter schmelzen, nicht zu warm werden lassen
* Ei-Ersatz anrühren und ziehen lassen

## Teig zubereiten

* Wichtig: Teig insgesamt nur wenig rühren
* Trockene Zutaten abwiegen und vermengen
* Joghurt dazu geben und rühren, bis es eine halbwegs glatte Menge ist
* Eiersatz und Butter dazu geben
* Zuletzt die gefrorenen Blaubeeren nur kurz unterheben

## Backen

* Im Konvektomaten: 35 Minuten bei 175°C ohne Wasser
