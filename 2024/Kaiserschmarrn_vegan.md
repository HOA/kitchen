# Kaiserschmarrn, vegan

**22 Personen**

---

- *3000 ml* Mandelmilch
- *500 ml* Mineralwasser Classic
- *1400 g* Weizenmehl 450
- *700 g* Dinkelmehl
- *46 g* Vanillin-Zucker
- *100 g* Backpulver
- *460 ml* neutrales Öl
- *345 g* Zucker
- *46 g* Salz
- *575 g* Rosinen
- *575 ml* Orangensaft
- *2200 ml* vegane Vanillesauce (fertig zubereitet)

---

* Rosinen in Orangensaft einlegen, am Besten für 1 Stunde oder länger
* Restliche Zutaten außer Mineralwasser zu einem Teig vermengen
* Mineralwasser unter minimalen Rühren in den Teig einbringen
* 3 GN 1x1 buttern und Teig (jeweils ca 2 Liter) einfüllen
* Rosinen abtropfen lassen und über den Teig verteilen
* Mindestens 30 Minuten bei 180° ohne Deckel im Konvektomaten backen
