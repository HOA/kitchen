# Rührei, vegan

**6 Personen**

---

- *400 g* Naturtofu
- *30g* Zwiebel (optional: gehackt)
- *0.5* Paprika
- *100g* Zucchini
- *60ml* Bratöl
- *100g* Sojajoghurt (ungesüßt)
- *2g* Hefeflocken
- *0.05* Muskatnuss
- *1g* Paprikapulver edelsüß
- *2g* Pfeffer
- *4g* Kurkuma
- *7g* Kala Namak
- Gehackter Schnittlauch, tiefgekühlt
- *2g* Salz
- *2 ml* Weißweinessig (5% Säure)

---

* Tofu bröseln und ausdrücken.
* Gemüse schneiden und einzeln anbraten.
* Tofo einzeln anbraten.
* Gemüse, Tofu und Gewürze vermengen.
* Sojajoghurt zugeben und 3 Minuten ziehen lassen.
* Kala Namak und Essig erst direkt vor dem Servieren dazu geben.
