# Rührei, Tier

**6 Personen**

---

* *600g* Vollei
* *30g* Zwiebel (optional: gehackt)
- *2g* Pfeffer
- *1g* Paprikapulver edelsüß
- *60ml* Bratöl
- Gehackter Schnittlauch, tiefgekühlt
- *2g* Salz

---
