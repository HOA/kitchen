# Veganer Linseneintof zum einfrieren

*Beilage, Hauptgericht, Suppe, Deutsch, Gemüsesuppe, Linseneintopf, Linsensuppe*

**4 Portionen**

---

- *2 EL* Pflanzenöl zum Braten
- *100 g* Zwiebel, gewürfelt
- *0.5 Stange* Porree gewürfelt
- *300 g* Karotten, gewürfelt
- *200 g* Knollensellerie, gewürfelt
- *200 g* Räuchertofu und/oder vegane Würstchen
- *250 g* trockene Linsen, braune oder grüne
- *1 l* vegane Gemüsebrühe
- *1 EL* Senf
- *1* Lorbeerblatt
- *0.25 TL* Muskat
- *1 TL* Salz
- *0.5 TL* Pfeffer
- *2 EL* Weißweinessig oder Apfelessig
- Zucker
- *20 g* Petersilie, gehackt

---

1. Das Öl in einem Suppentopf erhitzen. Zwiebeln, Porree, Karotten hinzugeben
   und 3-5 Minuten anbraten. (Wer möchte, kann noch Räuchertofu-Würfel oder
   vegane Würstchen anbraten).
2. Währenddessen die Linsen in ein feinmaschiges Sieb geben und unter fließendem
   Wasser abspülen. Anschließend abtropfen lassen.
3. Dann Kartoffeln, Sellerie und Linsen mit in den Topf geben. Gemüsebrühe
   eingießen. Senf, Lorbeerblatt und Muskat hinzugeben und alles aufkochen
   lassen. Einen Deckel auf den Topf geben und die Suppe ca. 30 Minuten köcheln
   lassen (je nachdem wie lange die Linsen benötigen, siehe Packungsanleitung).
4. Mit Weißweinessig, Salz und Pfeffer abschmecken. Zum Schluss die Petersilie
   hinzugeben und den Eintopf in Schalen servieren. Dazu schmeckt auch eine
   Scheibe frisch gebackenes Brot oder Bagels.
5. Guten Appetit!

Der Linseneintopf schmeckt am nächsten Tag, nachdem er durchgezogen ist, sogar
noch besser! Reste halten sich im Kühlschrank 3-5 Tage. Man kann ihn außerdem
prima einfrieren, auch portionsweise für Meal Prep. Weitere Ideen und Tipps zum
Rezept stehen oben im Blog-Beitrag.