# Vegane Mac and Not-Cheese

*vegan, vegetarisch, Januar, Februar, März, April, Mai, Juni, Juli, August, September, Oktober, November, Dezember, Nudeln*

**4 Personen (Hauptgericht), 6 Personen (Beilage)**

---

## Sauce

- *175g* Kartoffeln
- *50g* Karotten
- *60g* Zwiebeln
- *125ml* kochendes Wasser
- *85g* Cashews
- *100g* pflanzliche Margarine
- *10g* Hefeflocken (auf Melasse-Basis => Glutenfrei, z.B. https://www.amazon.de/Naturata-Bio-Melasse-Hefeflocken-100/dp/B01414GTYQ/)
- *20 ml* Zitronensaft
- *5 g* Tomatenmark
- *9 g* Salz
- *2 g* Knoblauchpulver
- *2 g* Paprikapulver, edelsüß

## Nudeln

- *500g* Nudeln

---

Die Kartoffeln, Karotten und Zwiebeln klein schneiden und zusammen mit den Cashews ca. 8-10 Minuten in Wasser kochen (bis sie weich sind).

Alle Zutaten in einen Blender geben und pürieren, bis eine glatte Creme  (Bei Bedarf Portionsweise). Für das kochende Wasser das Gemüsewasser nehmen.

Mit den gekochten Nudeln vermischt servieren.